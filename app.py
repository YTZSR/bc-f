from flask import Flask, request
import requests
from Blockchain import Blockchain
from Block import Block
import time
import json
import Users
import Transactions
import nft
import pickle
import config
import os

app = Flask(__name__)

tx = Blockchain()
users = Blockchain()
nfts = Blockchain()
peers = set()

CONNECTED_NODE_ADDRESS = "http://127.0.0.1:5000"

nfts.op = os.path.join(config.UPLOAD_FOLDER, config.NFT_INDEX)

posts = []

def verify_admin():
    # TODO
    return True

# User API
@app.route('/users/new_user', methods=['POST'])
def new_user():
    if not verify_admin():
        return "Forbidden", 403

    data = Users.new_users(users, request)

    response = app.response_class(
        response=json.dumps(data[0]),
        status=data[1],
        mimetype='application/json'
    )
    return response

@app.route('/users/chain', methods=['GET'])
def get_users_chain():
    return Users.get_users(users)

@app.route('/users/pending_user', methods=['GET'])
def get_pending_user():
    return json.dumps(users.unconfirmed_info)

@app.route('/users/mine', methods=['GET'])
def mine_unconfirmed_users():
    return Users.mine_unconfirmed_users(users, peers)

@app.route('/users/add_block', methods=['POST'])
def users_add_block():
    return Users.users_add_block(users, request)

@app.route('/users/tx', methods=['POST'])
def users_get_tx():
    user_data = request.get_json()
    account = user_data['public_key']

    if not account in Users.get_users_list(users):
        return "Invalid Account", 400

    list = Users.get_tx(account, tx)
    return json.dumps(list)

@app.route('/users/export', methods=['POST'])
def users_export():
    with open(config.export_address + '/user.bc', 'wb') as config_dictionary_file:
        # Step 3
        pickle.dump(users, config_dictionary_file)
    return "Success", 201

@app.route('/users/import', methods=['POST'])
def users_import():
    with open(config.export_address + '/user.bc', 'rb') as config_dictionary_file:
        global users
        users = pickle.load(config_dictionary_file)
    return "Success", 200

@app.route('/users/difficulty', methods=['POST'])
def set_difficulty():
    difficulty = request.get_json()['difficulty']
    users.present_difficulty = difficulty
    return "Success", 200


# Transaction API
@app.route('/tx/new_transaction', methods=['POST'])
def new_transaction():
    data = Transactions.new_transaction(tx, users, request)
    response = app.response_class(
        response=json.dumps(data[0]),
        status=data[1],
        mimetype='application/json'
    )
    return response

@app.route('/tx/chain', methods=['GET'])
def get_chain():
    return Transactions.get_chain(tx)

@app.route('/tx/pending_tx', methods=['GET'])
def get_pending_tx():
    return json.dumps(tx.unconfirmed_info)

@app.route('/tx/mine', methods=['GET'])
def mine_unconfirmed_tx():
    return Transactions.mine_unconfirmed_tx(tx, peers)

@app.route('/tx/add_block', methods=['POST'])
def tx_add_block():
    return Transactions.tx_add_block(tx, request)

@app.route('/tx/export', methods=['POST'])
def tx_export():
    with open(config.export_address + '/tx.bc', 'wb') as config_dictionary_file:
        # Step 3
        pickle.dump(users, config_dictionary_file)
    return "Success", 201

@app.route('/tx/import', methods=['POST'])
def tx_import():
    with open(config.export_address + '/tx.bc', 'rb') as config_dictionary_file:
        global tx
        tx = pickle.load(config_dictionary_file)
    return "Success", 200


# NFT API
@app.route('/nfts/new_file', methods=['POST'])
def new_nft_file():
    data = nft.new_file(nfts, request)
    response = app.response_class(
        response=json.dumps(data[0]),
        status=data[1],
        mimetype='application/json'
    )
    return response

@app.route('/nfts/transfer', methods=['POST'])
def transfer():
    data = nft.transfer(nfts, request)
    response = app.response_class(
        response=json.dumps(data[0]),
        status=data[1],
        mimetype='application/json'
    )
    return response


@app.route('/nfts/pending_nfts')
def get_pending_nfts():
    print(nfts.unconfirmed_info)
    return json.dumps(nfts.unconfirmed_info)


@app.route('/nfts/mine', methods=['GET'])
def mine_unconfirmed_nfts():
    return nft.mine_unconfirmed_nfts(nfts, peers)


@app.route('/nfts/chain', methods=['GET'])
def get_nft_chain():
    return nft.get_chain(nfts)


@app.route('/nfts/add_block', methods=['POST'])
def nft_add_block():
    return nft.nft_add_block(nfts, request)





# Endpoint to add new peers to the network
@app.route('/register_node', methods=['POST'])
def register_new_peers():
    # The host address to the peer node
    node_address = request.get_json()["node_address"]
    if not node_address:
        return "Invalid data", 400

    # Add the node to the peer list
    peers.add(node_address)

    # Return the blockchain to the newly registered node so that it can sync
    return get_chain()


# 创立新的节点
@app.route('/register_with', methods=['POST'])
def register_with_existing_node():
    """
    Internally calls the `register_node` endpoint to
    register current node with the remote node specified in the
    request, and sync the blockchain as well with the remote node.
    """
    node_address = request.get_json()["node_address"]
    if not node_address:
        return "Invalid data", 400

    data = {"node_address": request.host_url}
    headers = {'Content-Type': "application/json"}

    # Make a request to register with remote node and obtain information 放入节点名单
    response = requests.post(node_address + "/register_node",
                             data=json.dumps(data), headers=headers)

    if response.status_code == 200:
        global blockchain
        global peers
        # update chain and the peers
        chain_dump = response.json()['chain']
        blockchain = create_chain_from_dump(chain_dump)
        peers.update(response.json()['peers'])
        return "Registration successful", 200
    else:
        # if something goes wrong, pass it on to the API response
        return response.content, response.status_code


def create_chain_from_dump(chain_dump):
    blockchain = Blockchain()
    for idx, block_data in enumerate(chain_dump):
        block = Block(block_data["_Block__index"],
                      block_data["_Block__info"],
                      block_data["_Block__timestamp"],
                      block_data["_Block__previous_hash"])
        block.nonce = block_data["nonce"]
        proof = block.compute_hash()

        if idx > 0:
            added = blockchain.add_block(block, proof)
            if not added:
                raise Exception("The chain dump is tampered!!")
        else:  # the block is a genesis block, no verification needed
            blockchain.chain.append(block)
    return blockchain


if __name__ == '__main__':
    app.run()
