from Block import Block
import time
import pickle

class Blockchain:
    # difficulty = 2
    # op = None

    def __init__(self):
        """
        Constructor for the `Blockchain` class.
        """
        self.unconfirmed_info = []
        self.__chain = []
        self.__create_genesis_block()
        self.__bc_idx = {}
        self.__present_difficulty = 2
        self.__op = None

    def __create_genesis_block(self):
        """
        A function to generate genesis block and appends it to
        the chain. The block has index 0, previous_hash as 0, and
        a valid hash.
        """
        genesis_block = Block(0, [], time.time(), "0")
        self.proof_of_work(genesis_block)
        self.__chain.append(genesis_block)

    @property
    def last_block(self) -> Block:
        """
        A quick pythonic way to retrieve the most recent block in the chain. Note that
        the chain will always consist of at least one block (i.e., genesis block)
        """
        return self.__chain[-1]

    @property
    def chain(self):
        return self.__chain

    @property
    def present_difficulty(self):
        return self.__present_difficulty

    @present_difficulty.setter
    def present_difficulty(self, present_difficulty):
        self.present_difficulty = present_difficulty

    @property
    def bc_idx(self):
        return self.__bc_idx

    def save_bc_index(self):
        """
        :param op: the index file name defined in config.py
        """
        with open(self.op, "wb") as f:
            pickle.dump(self.__bc_idx, f)
        f.close()

    def load_bc_index(self):
        try:
            f = open(self.op, "rb")
        except OSError:
            print("The index file of current blockchain does not exist")
            return False
        else:
            previous_bc_idx = pickle.load(f)
            f.close()
        return previous_bc_idx

    def proof_of_work(self, block):
        """
        Function that tries different values of the nonce to get a hash
        that satisfies our difficulty criteria.
        """
        block.nonce = 0

        computed_hash = block.compute_hash()
        while not computed_hash.startswith('0' * block.difficulty):
            block.nonce += 1
            computed_hash = block.compute_hash()

        return computed_hash

    def add_block(self, block, proof):
        """
        A function that adds the block to the chain after verification.
        Verification includes:
        * Checking if the proof is valid.
        * The previous_hash referred in the block and the hash of
          a latest block in the chain match.
        """
        previous_hash = self.last_block.compute_hash()

        if previous_hash != block.previous_hash:
            return False
        # check difficulty
        if not self.is_valid_proof(block, proof):
            return False

        # block.hash = proof
        self.__chain.append(block)
        return True

    def is_valid_proof(self, block, block_hash):
        """
        Check if block_hash is valid hash of block and satisfies
        the difficulty criteria.
        """
        return (block_hash.startswith('0' * block.difficulty) and
                block_hash == block.compute_hash())


    def add_new_info(self, info):
        self.unconfirmed_info.append(info)

    def mine(self):
        """
        This function serves as an interface to add the pending
        transactions to the blockchain by adding them to the block
        and figuring out proof of work.
        """
        if not self.unconfirmed_info:
            return False

        last_block = self.last_block

        new_block = Block(index=last_block.index + 1,
                  info=self.unconfirmed_info,
                  timestamp=time.time(),
                  previous_hash=last_block.compute_hash(),
                  difficulty=self.present_difficulty)

        proof = self.proof_of_work(new_block)
        self.add_block(new_block, proof)
        self.unconfirmed_info = []
        return new_block.index


